import csv
import re
import unittest
from datetime import datetime, timedelta

from lib.residence import process_reader
from lib.utils import format_date, now_date


class TestResidence(unittest.TestCase):

    def test_has_one_entry_per_name(self):
        reader = csv.reader([
            'doesNot, lookAtThese',
            '2017-02-13,"Peter"',
            '2017-02-14,"Peter,Adam"',
        ])
        data = process_reader(reader)
        self.assertEqual(data.keys(), {'Adam', 'Peter'})

    def test_collects_all_dates_for_each_person(self):
        reader = csv.reader(
            [
                'doesNot, lookAtThese',
                '2017-02-13,"Peter"',
                '2017-02-14,"Peter,Adam"',
                '2017-02-15,"Adam"',
                '2017-02-16,"Adam,James"',
                '2017-02-17,"James"',
            ]
        )
        data = process_reader(reader)

        def dates_for(name):
            """Extract and format dates for a person by name"""
            return [format_date(date) for date in data[name]]

        self.assertEqual(dates_for('Peter'), ['2017-02-13', '2017-02-14'])
        self.assertEqual(dates_for('Adam'), ['2017-02-14', '2017-02-15', '2017-02-16'])
        self.assertEqual(dates_for('James'), ['2017-02-16', '2017-02-17'])

    def test_allows_near_future_dates(self):
        near_future = now_date() + timedelta(days=2)
        reader = csv.reader([
            'IAmA, header',
            '{},"Jane"'.format(format_date(near_future)),
        ])
        data = process_reader(reader)
        self.assertEqual(data['Jane'][0], near_future)

    def test_complains_for_far_future_dates(self):
        far_future = datetime.now() + timedelta(weeks=4)
        reader = csv.reader([
            'stupid, headers',
            '{},"Alex"'.format(format_date(far_future)),
        ])
        with self.assertRaisesRegex(
            Exception,
            re.escape("Woah are you crazy, [{}] is too far into the future".format(format_date(far_future))),
        ):
            process_reader(reader)

    def test_complains_with_empty_names(self):
        reader = csv.reader([
            'whatAreThey, goodFor',
            '2017-02-16,"Alex,"',
        ])
        with self.assertRaisesRegex(
            Exception,
            re.escape("No empty names please!"),
        ):
            process_reader(reader)

    def test_complains_with_invalid_dates(self):
        reader = csv.reader([
            'absolutely, nothing',
            'not-a-fucking-date,"Margaret"',
        ])
        with self.assertRaisesRegex(
            Exception,
            re.escape("Invalid date format [not-a-fucking-date]"),
        ):
            process_reader(reader)

    def test_complains_with_similar_names(self):
        reader = csv.reader([
            'date, name',
            '2017-02-16,"Alex"',
            '2017-02-16,"alëx"',
        ])
        with self.assertRaisesRegex(
            Exception,
            re.escape("Please do not have confusingly similar names"),
        ):
            process_reader(reader)
