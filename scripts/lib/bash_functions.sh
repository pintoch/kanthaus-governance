#!/bin/bash

_identifyBasePath () {
    repo=$1

    # find all git repos which contain the string in the config
    # and pick the first one
    config_file=$(grep -l "$repo" \
        ../../*/.git/config \
        $HOME/*/.git/config  \
        2>/dev/null \
        | head -n 1
    )

    if [ -f "$config_file" ]; then
        # output path
        realpath $(dirname "$config_file")/..
    else
        printf "\e[1m%s not found:\e[0m please enter path to it manually, or press 'enter' to exit (and let Doug know you had to do this)\n" "$repo"
        read -r userInput
        if [ ! -d "$userInput" ]; then
            printf "\e[1mExiting:\e[0m path to %s not entered\n" "$slug"
            exit
        fi
        # output
        realpath $userInput
    fi
}

