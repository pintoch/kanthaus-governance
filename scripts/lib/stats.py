from datetime import datetime, timedelta
from lib.influx import get_electricity_import, get_electricity_export, get_electricity_production, get_outdoor_temperature, get_heating_electricity, get_water_usage
from lib.base import MissingValueError
import subprocess
import json
import time
from lib.kanthaus_private import credentials

ELECTRICITY_IMPORT_PRICE = 0.3050  # € / kWh
ELECTRICITY_EXPORT_PRICE = 0.0809  # € / kWh
# Approximative estimate of the mix of buying renewables power in priority
# when our own solar is down (with wind around 0.012), and falling back on what
# the German grid has to offer when wind and solar is down (oscillating between 0.200 and 0.450)
ELECTRICITY_EMISSIONS_IMPORT =  0.200 # kg CO2e / kWh
# Median estimate for rooftop solar by IPCC
ELECTRICITY_EMISSIONS_SOLAR =  0.041 # kg CO2e / kWh
WATER_PRICE = 4.5  # € / m³
WATER_EMISSIONS = 0.35 # kg CO2e / m³
USAGE_GRAPH_TIMESPAN_IN_DAYS = 30 * 3

def get_usage_graph_link():
    time_min = datetime.now() - timedelta(days=USAGE_GRAPH_TIMESPAN_IN_DAYS)
    time_max = datetime.now()

    subprocess.run([
        "curl",
        "-s",
        "-H",
        "Authorization: Bearer {}".format(credentials['grafana_token']),
        "--output",
        "/tmp/graph.png",
        "https://grafana.yunity.org/render/d-solo/QFAKjLPWk/coordination-meeting-rendering?orgId=4&from={}000&to={}000&theme=light&panelId=2&width=600&height=300&tz=Europe%2FBerlin".format(
            time_min.strftime('%s'),
            time_max.strftime('%s')
        ),
    ])
    res = subprocess.run([
        "curl",
        "-s",
        "https://pad.kanthaus.online/uploadimage",
        "-F"
        "image=@/tmp/graph.png"
    ], capture_output=True)
    d = json.loads(res.stdout.decode('utf8'))
    return d['link']


def get_stats(people, days=7, offset=0):

    time_min = datetime.now() - timedelta(days=days + offset)
    time_max = datetime.now() - timedelta(days=offset)
    spentnights = 0
    for person in people:
        for date in person.dates:
            if date >= time_min and date < time_max:
                spentnights += 1

    # ------------------------
    # electricity
    # ------------------------
    electricity_import = get_electricity_import(days=days, offset=offset) / 1000
    electricity_export = get_electricity_export(days=days, offset=offset) / 1000
    electricity_production = get_electricity_production(days=days, offset=offset) / 1000
    electricity_heating = get_heating_electricity(days=days, offset=offset) / 1000

    electricity_usage = electricity_import + electricity_production - electricity_export
    electricity_selfusage = electricity_production - electricity_export

    electricity_cost = electricity_usage * ELECTRICITY_IMPORT_PRICE
    electricity_paid = electricity_import * ELECTRICITY_IMPORT_PRICE - electricity_export * ELECTRICITY_EXPORT_PRICE;

    electricity_self_rate = electricity_selfusage / electricity_usage
    electricity_emissions = electricity_import * ELECTRICITY_EMISSIONS_IMPORT + electricity_selfusage * ELECTRICITY_EMISSIONS_SOLAR

    heating_cost = electricity_heating * ELECTRICITY_IMPORT_PRICE

    # ------------------------
    # water
    # ------------------------
    try:
        water_usage = get_water_usage(days=days, offset=offset)
    except MissingValueError as e:
        print("{}: water usage missing".format(e))
        water_usage = 0
    water_cost = water_usage * WATER_PRICE
    water_emissions = water_usage * WATER_EMISSIONS

    # ------------------------
    # temperature
    # ------------------------
    try:
        outdoor_temperature = get_outdoor_temperature(days=days, offset=offset)
    except KeyError:
        outdoor_temperature = None

    return {
        "spentnights": spentnights,
        "outdoor_temperature": outdoor_temperature,
        "electricity_usage": electricity_usage,
        "electricity_cost": electricity_cost,
        "electricity_paid": electricity_paid,
        "electricity_self_rate": electricity_self_rate,
        "electricity_emissions": electricity_emissions,
        "heating_cost": heating_cost,
        "water_usage": water_usage,
        "water_cost": water_cost,
        "water_emissions" : water_emissions,
    }
