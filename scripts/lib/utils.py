from datetime import datetime
import unicodedata


def unidecode(str):
    # unidecode does this nicely, but needs installing seperately
    # https://stackoverflow.com/a/517974
    nfkd_form = unicodedata.normalize('NFKD', str)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])


def now_date():
    """Same as datetime.now() but with 0 hour, minute, second, and microsecond values

    In other words, the last midnight that occured.
    """
    return datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)


def format_date(date):
    return date.strftime("%Y-%m-%d")


def first(items, default=None):
    return next(iter(items), default)


def last(items, default=None):
    return next(reversed(items), default)


def group_dates(dates):
    """Group dates into group of subsequents dates.
    
    (It is not about dates of groups of people
    but about groups of dates!)
    
    Assumes that dates are sorted chronologically.
    
    Returns:
        list date groups saved as lists of two elements: start and end date
    """
    if len(dates) == 0:
        return []
    
    ranges = []
    start_date = dates[0]
    previous_date = dates[0]
    for date in dates[1:]:
        diff = date - previous_date
        if diff.days > 1:
            ranges.append([start_date, previous_date])
            start_date = date

        previous_date = date

    ranges.append([start_date, previous_date])

    return ranges
