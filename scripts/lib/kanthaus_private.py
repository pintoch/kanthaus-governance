import os
import yaml
import subprocess
from os.path import join

def get_file_online(filename):
    ps = subprocess.Popen("git archive --remote=git@gitlab.com:kanthaus/kanthaus-private.git HEAD {} | tar -xOf -".format(filename).strip(), shell=True, stdout=subprocess.PIPE)
    return ps.stdout.read().decode('utf-8') 

def get_file_local(dir, filename):
    with open(join(dir, filename)) as f:
      return f.read()

def get_file(filename):
    KANTHAUS_PRIVATE_DIR = os.environ.get('KANTHAUS_PRIVATE_DIR')
    if KANTHAUS_PRIVATE_DIR:
        return get_file_local(KANTHAUS_PRIVATE_DIR, filename)
    else:
        return get_file_online(filename)

def get_credentials():
    return yaml.load(get_file("credentials.yaml"), Loader=yaml.Loader)
    

credentials = get_credentials()

def get_evaluation_file():
    return get_file("evaluationRecord.csv")

def get_residence_file():
    return get_file("residenceRecord.csv")