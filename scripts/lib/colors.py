# from https://stackoverflow.com/a/287944
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    GRAY = '\033[90m'


def ok(str):
    return bcolors.OKGREEN + str + bcolors.ENDC


def bold(str):
    return bcolors.BOLD + str + bcolors.ENDC


def fail(str):
    return bcolors.FAIL + str + bcolors.ENDC


def warning(str):
    return bcolors.WARNING + str + bcolors.ENDC


def underline(str):
    return bcolors.UNDERLINE + str + bcolors.ENDC


def faded(str):
    return bcolors.GRAY + str + bcolors.ENDC
