#!/bin/bash

cd "$(dirname $0)" # folder of the script

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    echo "Usage: $(basename "$0") [-h]"
    echo ""
    echo "Generates autofilled CoMe pad for next week"
    echo ""
    echo "optional arguments:"
    echo "  -h, --help            show this help message and exit"
    exit 0
fi

sleep 0.5
echo "INFO: uses the last CoMe website page - make sure most recent CoMe already uploaded!"
sleep 0.5
echo "INFO: assumes next CoMe will occur on next Monday"
sleep 0.5
echo -n "Continue? (y/n)"
read -r answer
if [ "$answer" != "${answer#[Yy]}" ]; then

  # Copy template
  cp generateNextCoMePad_template /tmp/nextCoMePad
  
  # Copy last CoMe, iterating back from today to last YYYY-MM-DD_CoMe/item.md
  if [ -f /tmp/lastCoMe ]; then
    rm /tmp/lastCoMe
  fi
  lastCoMeDate=$(date -I)
  while [ ! -s /tmp/lastCoMe ]; do
    curl -f -s https://gitlab.com/kanthaus/kanthaus.online/-/raw/master/user/pages/40.governance/90.minutes/"$lastCoMeDate"_CoMe/item.md \
      > /tmp/lastCoMe
    lastCoMeDate=$(date -d "${lastCoMeDate} - 1 days" '+%Y-%m-%d')
  done
  lastCoMe="/tmp/lastCoMe"
  
  # Calculate and substitute next come date
  # N.b. '+ 8 days' since while loop iterates one extra day
  nextCoMeDate=$(date --date='10:00 next Mon' +%F)
  sed -e "s/{{NEXT_COME_DATE}}/${nextCoMeDate}/" -i "/tmp/nextCoMePad"
  
  # Calc and sub next come number
  coMeNumber=$(grep "# CoMe #" "$lastCoMe" | tr -cd "[:digit:]")
  if [ -n "$coMeNumber" ]; then
    nextCoMeNumber=$((coMeNumber + 1))
  else
    nextCoMeNumber="{COME NUMBER NOT FOUND!}"
    echo "$nextCoMeNumber"
  fi
  sed -e "s/{{NEXT_COME_NUMBER}}/${nextCoMeNumber}/" -i "/tmp/nextCoMePad"
  
  # Find and sub next facilitator
  # N.b. there are two '- 10:00 CoMe' fields: this selects last
  nextCoMeFacilitator=$(grep "\ 10:00 CoMe" "$lastCoMe" | cut -d "[" -f2 | cut -d "]" -f1 | tail -1)
  if [ -z "$nextCoMeFacilitator" ]; then
    nextCoMeFacilitator="{NEXT FACILITATOR NOT FOUND!}"
    echo "$nextCoMeFacilitator"
  fi  
  sed -e "s/{{NEXT_COME_FACILITATOR}}/${nextCoMeFacilitator}/" -i "/tmp/nextCoMePad"
  
  # Find and sub in to-dos (if existing) from last week
  todosLastCoMe=$(sed '1,/^## 4. To do$/d' "$lastCoMe" | sed '/## 5. Discussion & Announcements/,$d')
  if [ -n "$todosLastCoMe" ]; then
    awk -v r="$todosLastCoMe" '{gsub(/{{TODOS_LAST_COME}}/,r)}1' "/tmp/nextCoMePad" > "/tmp/x"
    mv "/tmp/x" "/tmp/nextCoMePad"
  else
    sed -e 's/{{TODOS_LAST_COME}}/-/' -i "/tmp/nextCoMePad"
  fi
  
  # Find and sub in points (if existing) from last week
  fromLastCoMe=$(grep -E "## .* For next week" -A 10 "$lastCoMe" | tail -n +2)
  if [ -n "$fromLastCoMe" ]; then
    awk -v r="$fromLastCoMe" '{gsub(/{{FROM_LAST_COME}}/,r)}1' "/tmp/nextCoMePad" > "/tmp/x"
    mv "/tmp/x" "/tmp/nextCoMePad"
  else
    sed -e 's/{{FROM_LAST_COME}}/*/' -i "/tmp/nextCoMePad"
  fi
  
  # Output next come pad based on user toolage
  if command -v xclip &> /dev/null; then
    xclip "/tmp/nextCoMePad"
    printf "\e[1mNext week's pad copied to clipboard: middle-click to paste\e[0m\n"
  else
    printf "\e[1mReading pad contents to command line... \e[0m (Install 'xclip' for direct copying)\n."
    sleep 2
    cat "/tmp/nextCoMePad"
  fi
  
else

  echo "See you when you're ready."
  
fi
