#!/bin/bash

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    echo "Usage: $(basename "$0") [-h]"
    echo ""
    echo "Lists residence record of today and last 5 days + resident totals"
    echo ""
    echo "optional arguments:"
    echo "  -h, --help            show this help message and exit"
    exit 0
fi

echo "Getting data from gitlab.com:kanthaus/kanthaus-private.git..."
git archive --remote=git@gitlab.com:kanthaus/kanthaus-private.git HEAD residenceRecord.csv | tar -xOf - > /tmp/resRec
grep "$(date -I)" -B 5 /tmp/resRec > /tmp/resList
counter=1
while [ $counter -lt 8 ]; do
  ttlPpl=$(head -$counter /tmp/resList | tail -1 | tr -cd , | wc -c)
  if [ "$ttlPpl" -lt 2 ]; then
    ttlPpl=0
  fi
  sed -e "${counter}s/$/$(printf "\e[1m → Total: %s\e[0m/ " "${ttlPpl}")" -i /tmp/resList
  counter=$((counter + 1))
done
cat /tmp/resList
