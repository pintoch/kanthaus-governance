import numpy as np
import matplotlib
import matplotlib.pyplot as plt

place = ["inside, narrow", "inside space\noutside, narrow", "outside"]
behaviour = ["tiny contact\nlittle distance", "little contact\nwelcome hug\n", "cuddling\nacroyoga\nkissing\nmassage"]

#risk = np.array([[3, 5, 10],
                 #[2, 4, 10],
                 #[1, 2, 10]])

risk = np.array([[3, 4, 5],
                 [2, 3, 5],
                 [1, 2, 5]])

# 2020-10-24, added by Antonin
mask_discounted_risk = np.array(
                [[2, 3, 4],
                 [1, 2, 3],
                 [1, 1, 3]])

fig, ax = plt.subplots(figsize=(7.5, 5.5))
im = ax.imshow(risk, cmap='OrRd')

# We want to show all ticks...
ax.set_xticks(np.arange(len(behaviour)))
ax.set_yticks(np.arange(len(place)))
# ... and label them with the respective list entries
ax.set_xticklabels(behaviour)
ax.set_yticklabels(place)

ax.set_xlabel('Activity with the contact\n(Multiple contacts: chose highest)')
ax.set_ylabel('Location of meeting\n(Multiple contacts: chose highest)')

# Rotate the tick labels and set their alignment.
#plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         #rotation_mode="anchor")

# Loop over data dimensions and create text annotations.
for i in range(len(place)):
    for j in range(len(behaviour)):
        text = ax.text(j, i, risk[i, j],
                       ha="center", va="center", color="black", size="40")

        # 2020-10-24, added by Antonin
        # If the risk without a mask is different from the risk with a mask
        if risk[i, j] != mask_discounted_risk[i, j]:
            # add the risk with a mask in brackets
            ax.text(j, i + .35, f'({mask_discounted_risk[i, j]})', ha='center', va='center', color='black', size='20')

ax.set_title("Contraction probability")

# 2020-10-24: added by Antonin
plt.figtext(0.5, 0, 'Numbers in brackets indicate points if a face mask was worn', horizontalalignment='center')

# Add table number (to ease reference from the framework text)
plt.figtext(0.05, 0.05, '2', horizontalalignment='left', size='25')

fig.tight_layout()
plt.savefig('transmission.svg')
plt.show()

