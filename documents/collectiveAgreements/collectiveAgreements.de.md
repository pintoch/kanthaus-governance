# Gemeinschaftliche Vereinbarungen

### Koordinationstreffen
Wir treffen uns Montags um 10:00 um Informationen zu teilen, Pläne zu machen und grundsätzlich zu koordinieren. Allen Anwesenden wird empfohlen dabeizusein.

===

### Coronavirus
Alle Menschen, die zu uns kommen möchten, müssen entweder komplett gegen COVID-19 geimpft sein, innerhalb der letzten 6 Monate genesen sein, einen negativen PCR Test vorweisen können, der weniger als 48 Stunden alt ist, oder einen negativen Schnelltest, der innerhalb der letzten 24 Stunden gemacht wurde.

### Englisch als Standard
Wir sind ein internationales Projekt und wollen, dass Menschen, die kein Deutsch sprechen, an unserem Alltag teilhaben können. Deshalb ist Englisch unser sprachlicher Standard, sowohl online als auch im Haus. Deutsch wird notwendigerweise in bürokratischen und technischen Bereichen verwendet: Frag nach einer Übersetzung, falls du interessiert bist und dir keine angeboten wurde. Wir lieben alle anderen Sprachen ebenso, aber erwarten von Menschen, dass sie sich bewusst darüber sind, dass andere Sprachen meistens exklusiver sind. In unserer Alltagskommunikation verwenden wir eine Menge kreativer Sprachmischungen (Denglisch, Franglais) und machen das gern auch weiterhin so. :)

### Power Hour
Wir treffen uns Donnerstags um 10:00 Uhr, um kurz gemeinschaftliche Reinigungsaufgaben zu überblicken, zu priorisieren und aufzuteilen. Dann trennen wir uns und putzen für eine Stunde. Dies ist eine Ergänzung zu normalem Putzen, kein Ersatz. Allen Anwesenden wird empfohlen dabeizusein.

### Sozialsauna
Alle vier Wochen treffen wir uns, um soziale Spannungen und Konflikte im Haus zu besprechen. Es geht nicht in erster Linie darum, Probleme zu lösen, sondern darum einen Raum zu schaffen, in dem Kanthausians über alles sprechen können, was das Gemeinschaftsleben trübt. Besuchende sind nicht eingeladen. Freiwilligen und Mitgliedern wird empfohlen teilzunehmen.

In den Wochen, in denen keine Sozialsauna stattfindet, reservieren wir einen Zeitslot für einen optionalen Heartsharing Circle (ein Raum, um Gedanken und Gefühle zu teilen), den alle Anwesenden besuchen können.

### Hosting-Pflicht
Hosts - Freiwillige oder Mitglieder, die zustimmen Besuchende zu hosten - sind verantwortlich, dafür dass sowohl die Besuchenden, als auch die anderen Anwesenden die bestmögliche Zeit verbringen können. Diese Verantwortung beinhaltet Aufgaben wie das Geben einer Haustour, Fragen zu beantworten, dabei zu helfen häufige Probleme zu vermeiden und Spannungen zu lösen. Hosts sind ebenfalls dafür verantwortlich eine Check-In Runde zu organisieren, wenn ihre Besuchenden ~7 Tage im Kanthaus verbracht hat. Die Check-In Runde ist eine zwanglose Unterhaltung, in der die Person, deren Host und 1-2 weitere Freiwillige oder Mitglieder schauen, wie es für die Besucher:in sowie die anderen Anwesenden so läuft. Die Hosting-Pflicht sollte explizit übertragen werden, wenn der momentane Host für mehr als 2-3 Tage weg ist.

### Ruhezeiten
Wir wollen, dass alle gut schlafen können. Die generelle Richtlinie ist zwischen 00:00 und 7:00 Uhr so leise zu sein wie möglich, vor allem (in der Nähe von) dort wo Menschen schlafen in K20-2 und K22-2. Laute Tätigkeiten sollten nur zwischen 9:00 und 21:00 Uhr verrichtet werden. Erhebliche Abweichungen von dieser Richtlinie sollten mit Vorlaufzeit vorgeschlagen werden, am besten im Koordinationstreffen.

### Freie Tische
Bitte hilf uns dabei die gemeinsamen Tischflächen möglichst nutzbar zu halten. Generell bedeutet das, die Flächen frei zu räumen sobald du fertig bist. Falls du Dinge auf einem Tisch lassen musst, versuch es auf eine platzsparende Weise zu tun, indem du bspw. Papiere auf deinen Laptop obendrauf legst. Die Tische im Wohnzimmer und in der Küche sollten immer frei sein: Lass nichts auf diesen Tischen liegen und wisch sie einmal ab, wenn sie nicht bereits sehr sauber sind.

### Freie Fensterbänke
Jeder Raum muss über mindestens ein leicht zu öffnendes Fenster verfügen: bitte lass nichts auf der Fensterbank liegen, die gerade frei ist.

### Lass die Fenster heile
Bitte benutze die Fensterhalter. Falls es keinen Fensterhalter gibt oder es nicht möglich ist ihn zu verwenden, dann lass das Fenster nicht unbeobachtet oder finde einen anderen Weg es davon abzuhalten zu zu knallen.

### Rauchen
Rauche nicht innerhalb der Gebäude. Wenn Du draußen rauchst, achte bitte darauf, dass der Rauch und Geruch nicht durch offene Türen oder Fenster ins Gebäude gelangt. Wirf keine Kippenstummel auf den Boden! Du kannst sie im schwarzen Mülleimer im Garten entsorgen.


### Alkohol
Kanthaus ist keine Bar: bitte komme nicht mit Hauptziel Alkohol zu trinken vorbei und sei dir bewusst, dass Alkoholkonsum nicht als Entschuldigung oder Rechtfertigung für dein Verhalten angesehen wird. Das heißt auch, dass verantwortungsbewusster Alkoholkonsum toleriert wird. Bitte frage eine/einen der Freiwilligen oder Mitglieder, falls du mehr Fragen oder Bedenken bezüglich dieses Themas hast.


### Tiere
Tiere sind nicht unbedingt willkommen. Gründe hierfür sind beispielsweise: allergische Reaktionen auf Tiere, Haarverlust, Defäkation, Schäden an der Einrichtung, Bissigkeit und weitere nicht wünschenswerte Verhaltensweisen. Potenzielle tierische Anwesende werden von Fall zu Fall besprochen. Bitte wende Dich zur Absprache an ein Mitglied, wenn Du ein Tier mitbringen möchtest.

### Änderungen der gemeinschaftlichen Vereinbarungen
Jede Ergänzung, Subtraktion oder andere Änderungen an diesem Dokument sollte nach der [Verfassung](../constitution#collagrchange) erfolgen.
