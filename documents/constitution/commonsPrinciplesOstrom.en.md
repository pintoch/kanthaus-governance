<figure style="font-family: serif">
    
<h3 style="text-align:center; padding: 0 20px 0 20px">Table 3.1. <i>Design principles illustrated by long-enduring CPR institutions</i></h3>

<hr style="border: 0; border-top: 3px double #8c8c8c; background-color: transparent">

1. Clearly defined boundaries  
	Individuals or households who have rights to withdraw resource units from the CPR must be clearly defined, as must the boundaries of the CPR itself.
	
2. Congruence between appropriation and provision rules and local conditions  
	Appropriation rules restricting time, place, technology, and/or quantity of resource units are related to local labor, material, and/or money.
	
3. Collective-choice arrangements  
	Most individuals affected by the operational rules can participate in modifying the operational rules.
	
4. Monitoring  
	Monitors, who actively audit CPR conditions and appropriator behavior, are accountable to the appropriators or are the appropriators.
	
5. Graduated sanctions  
	Appropriators who violate operational rules are likely to be assessed graduated sanctions (depending on the seriousness and context of the offense) by other appropriators, by officials accountable to these appropriators, or by both.
	
6. Conflict-resolution mechanisms  
	Appropriators and their officials have rapid access to low-cost local arenas to resolve conflicts among appropriators or between appropriators and officials.
	
7. Minimal recognition of rights to organize  
	The rights of appropriators to devise their own institutions are not challenged by external governmental authorities.

_For CPRs that fire parts of larger systems:_

8. Nested enterprises  
Appropriation, provision, monitoring, enforcement, conflict resolution, and governance activities are organized in multiple layers of nested enterprises.

<hr style="border: 0; border-top: 3px double #8c8c8c; background-color: transparent">

</figure>

<small>Ostrom, Elinor (1990). _[Governing the commons : the evolution of institutions for collective action.](https://www.actu-environnement.com/media/pdf/ostrom_1990.pdf)_ Cambridge: Cambridge University Press. p90. ISBN 0-521-37101-5.</small>
